from distutils.core import setup

setup(
    name='xdwim',
    version='0.1',
    scripts=['xdwim', 'xdwimctl'],
    license='MIT',
    long_description=open('README.md').read(),
)
